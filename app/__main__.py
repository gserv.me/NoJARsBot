# coding=utf-8
import asyncio
import logging
import os

from logging.handlers import TimedRotatingFileHandler
from time import sleep

from app.bot import Bot
from app.config import Config


__author__ = "Gareth Coles"
config = Config()


if not os.path.exists("logs"):
    os.mkdir("logs")

if not os.path.exists("tmp"):
    os.mkdir("tmp")

logging.basicConfig(
    format="%(asctime)s | %(name)s | [%(levelname)s] %(message)s",
    level=logging.INFO
)


def setup_logger(name):
    if not os.path.exists("logs/{}".format(name)):
        os.mkdir("logs/{}".format(name))

    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(
        filename="logs/{0}/{0}.log".format(name), encoding="utf-8",
        when="midnight", backupCount=30
    )
    handler.setFormatter(
        logging.Formatter(
            "%(asctime)s | %(name)s | [%(levelname)s] %(message)s")
    )

    logger.addHandler(handler)

    return logger


setup_logger("discord").setLevel(logging.WARN)
setup_logger("bot")


if not os.path.exists("config.yml"):
    print(
        "Unable to find config.yml - please copy config.yml.example "
        "and edit it!"
    )
    exit(1)


def run():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    bot = Bot(config, loop=asyncio.get_event_loop())
    bot.run(config.token, bot=True)


if config.auto_reconnect:
    print("Running in auto-reconnect mode.")

    while True:
        run()
        print("Reconnecting...")
        sleep(5)
else:
    print("Running without auto-reconnect.")
    run()
