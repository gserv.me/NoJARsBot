from os import environ

from ruamel.yaml import safe_load


class Config:
    def __init__(self):
        self.config = safe_load(open("config.yml"))

        self.token = self.config.get("token") or environ.get("TOKEN") or ""
        self.server = self.config.get("server") or environ.get("SERVER") or ""
        self.admin_channel = self.config.get("admin_channel") or environ.get("ADMIN_CHANNEL") or ""
        self.use_roles = self.config.get("use_roles", False)
        self.roles = self.config.get("roles", list())
        self.permissions = self.config.get("permissions", ["administrator"])
        self.auto_reconnect = self.config.get("auto_reconnect", True)
