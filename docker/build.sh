#!/bin/bash

# Build and deploy on master branch
if [[ $CI_COMMIT_REF_SLUG == 'master' ]]; then
    echo "Connecting to docker hub"
    echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin

    changed_lines=$(git diff HEAD~1 HEAD docker/Dockerfile | wc -l)

    echo "Dockerfile was changed"

    echo "Building..."
    docker build -t gdude2002/nojarsbot:latest -f docker/Dockerfile .

    echo "Pushing image to Docker Hub..."
    docker push gdude2002/nojarsbot:latest

#    echo "Deploying container"
#    curl -H "token: $AUTODEPLOY_TOKEN" $AUTODEPLOY_WEBHOOK
else
    echo "Skipping deploy"
fi
